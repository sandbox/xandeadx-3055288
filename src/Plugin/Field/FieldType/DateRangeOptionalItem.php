<?php

namespace Drupal\datetime_range_optional\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\datetime_range\Plugin\Field\FieldType\DateRangeItem;

class DateRangeOptionalItem extends DateRangeItem {

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties = parent::propertyDefinitions($field_definition);

    $properties['value']->setRequired(FALSE);
    $properties['end_value']->setRequired(FALSE);

    return $properties;
  }

}
